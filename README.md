
# RedditGiveawayHelper

Helping reddit raffles by replying to people that have entered with a confirmation message!

This script is a client that needs a server to function.

## Setup

 1. check if you have python3 installed:
   
    type `python3` in your terminal.
    If you have Python installed you should see something like this:
   
		Python 3.9.7 (default, Oct 12 2021, 22:38:23) 
		[Clang 13.0.0 (clang-1300.0.29.3)] on darwin
		Type "help", "copyright", "credits" or "license" for more information.
		>>>
    
    Make sure your version is at least 3.9.7

	**type `exit()` and hit Enter** to leave the Python shell. You can tell that you're in the Python shell from the leading `>>>` in your Terminal.
	
	#
	**If you don't have Python, install it:** [Download the latest stable version here!](https://www.python.org/downloads/)

	If the installer asks you if you want to add it to **PATH**, make sure to do so!

2. [Download the script .zip](https://gitlab.com/thunderFD/redditgiveawayhelper/-/archive/main/redditgiveawayhelper-main.zip) from this repository, unpack the .zip, and open your Terminal in the directory of the bot.

3. Install dependencies with `pip3 install -r requirements.txt`

	(make sure you're not in Python anymore, if you are type `exit()` and hit Enter.)

4. Configure config.ini with your login details and server URL.

5. Start the bot with `python3 bot.py`

The bot should now run with no errors!
