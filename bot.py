from typing import Optional, Tuple
import praw
import configparser
import requests
import time

# read config
config = configparser.RawConfigParser()
config.read("dev_config.ini")
if "credentials" not in config:
    config.read("config.ini")

user_agent: str = "Giveaway Helper"
client_id: str = config["credentials"]["client_id"]
client_secret: str = config["credentials"]["client_secret"]
user_name: str = config["credentials"]["user_name"]
user_password: str = config["credentials"]["user_password"]

request_url: str = config["server"]["request_url"]

cooldown_seconds: int = int(config["client"]["cooldown_seconds"])

# connect with Reddit
reddit = praw.Reddit(
    client_id=client_id,
    client_secret=client_secret,
    username=user_name,
    password=user_password,
    user_agent=user_agent,
    ratelimit_seconds=1200,  # type: ignore
)


def reply_to_comment(comment_id: str, reply_msg: str) -> None:
    global reddit
    comment = praw.models.Comment(reddit, comment_id)  # type: ignore
    comment.reply(reply_msg)


def get_job() -> Tuple[Optional[str], Optional[str]]:
    response = requests.get(request_url, timeout=10).json()

    if response["success"] and response["found_comment"]:
        comment_id = response["comment_id"]
        reply_message = response["reply_message"]

        return comment_id, reply_message

    return None, None


if __name__ == "__main__":
    print(f"User: {reddit.user.me()}")
    print(f"Reddit read only mode: {reddit.read_only}")

    while True:
        t = time.time()

        try:
            print("getting job from server.")
            # get comment_id and reply_msg from server
            comment_id, reply_msg = get_job()

            # no available comment... sleep
            if comment_id is None or reply_msg is None:
                print("No comment found, waiting 1s before requesting again")
                time.sleep(1)
                continue

            try:
                # reply to comment
                reply_to_comment(comment_id, reply_msg)
                print(
                    f"replied to comment {comment_id}. took {time.time()-t:.2f}s")
            except Exception as e:
                print(f"FAILED TO REPLY after {time.time()-t:.2f}s!\n{e}")
        except Exception as e:
            print("Something wrong happened", e)

        time.sleep(cooldown_seconds)
